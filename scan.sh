#!/bin/ash
# shellcheck shell=dash

set -eu -o pipefail

: "${REPO_URL:=https://gitlab.alpinelinux.org/alpine/aports.git}"
: "${APORT_TTL:=6}"
: "${SMTPD:=mailpit:1025}"
: "${MR_TITLE:=Remove aged packages from testing}"

readonly repo_dir=$HOME/aports
readonly months_to_release=3

# shellcheck disable=SC2016
setup_repo() {
	if [ -d "$HOME/aports/.git" ]; then
		echo "Updating aports"
		git -C "$repo_dir" checkout master
		if git -C "$repo_dir" show-ref --quiet refs/heads/aports-testing-ttl; then
			git -C "$repo_dir" branch -D aports-testing-ttl
		fi
		git -C "$repo_dir" reset --hard
		git -C "$repo_dir" pull
	else
		echo "Setting up aports repo"
		git clone "$REPO_URL" "$repo_dir"
		git -C "$repo_dir" config user.name "Alpine Infra"
		git -C "$repo_dir" config user.email "alpine-infra@alpinelinux.org"
		git -C "$repo_dir" config credential.helper \
			'!f() { sleep 1; echo "username=${GITLAB_USER}"; echo "password=${GITLAB_TOKEN}"; }; f'
		git -C "$repo_dir" commit-graph write --reachable --changed-paths
	fi
}

# shellcheck disable=SC2059
template() {
    local to="$1" aports="$2" uuid=
    uuid=$(cat /proc/sys/kernel/random/uuid)
    cat <<- EOF
	From: Alpine Linux Infra <alpine-infra@alpinelinux.org>
	To: $to
	Content-Type: text/plain; charset=utf-8
	Mime-Version: 1.0
	Message-ID: <$uuid@alpinelinux.org>
	Subject: Stale packages in testing repository

	Hello Alpine Linux contributor

	You have previously contributed to the Alpine Linux aports repository.
	The packages you have submitted to the testing repository are over
	$APORT_TTL months old and should be moved to our community repository
	or be removed from aports completely.

	Some basic information about the policies and workflow of our repositories
	can be found in the aports repository README.md. In the future this will be
	extended to our developer handbook which can be found at:
	https://docs.alpinelinux.org

	If you do not take action we will automatically remove these packages in
	3 months from now.

	Affected packages:

	$(printf "$aports")

	Thank you for your understanding.

	Alpine Linux Team
	EOF
}

send_email() {
	local maintainer="$1" aports="$2"
	echo "Sending email to: $maintainer"
	rcpt="${maintainer#*<}"
	rcpt="${rcpt%%>*}"
	template "$maintainer" "$aports" |
		curl --silent \
		--url "smtp://$SMTPD" \
		--mail-from "alpine-infra@alpinelinux.org" \
		--mail-rcpt "$rcpt" \
		--upload-file -
}

# shellcheck disable=SC3060
# we are not using dash
notify_maintainers() {
	local report age_limit age maintainer aport aports prevmaintainer prevaport
	report=$(mktemp)
	age_limit=$(($(date +%s) - (2628000 * APORT_TTL)))
	setup_repo
	cd "$repo_dir" || exit
	for aport in testing/*/APKBUILD; do
		echo "Getting timestamp for aport: $aport"
		age=$(git log --format='%ad' --date=format:'%s' -- "$aport" | tail -1)
		maintainer=$(awk -F': ' '/# *Maintainer/ {print $2}' "$aport")
		if [ "$age" -lt "$age_limit" ]; then
			case "$maintainer" in
				*[A-Za-z0-9]*\ \<*@*.*\>)
					printf '%s;%s\n' "$aport" "${maintainer//;}" >> "$report";;
			esac
		fi
	done

	echo "Sending notification emails"
	sort -k 2 -t';'  "$report" | while read -r line; do
		aport=${line%%;*}
		aport=${aport%/*}
		maintainer=${line#*;}
		if [ "$maintainer" = "${prevmaintainer:-}" ]; then
			if [ -z "${aports:-}" ]; then
				aports=$aport
			else
				aports="$aports\n$aport"
			fi
		elif [ -n "${prevmaintainer:-}" ] && [ "$maintainer" != "${prevmaintainer}" ]; then
			[ -z "${aports:-}" ] && aports=${prevaport:-}
			send_email "$prevmaintainer" "$aports"
			unset aports
		fi
		prevmaintainer=$maintainer
		prevaport=$aport
	done
	rm -f "$report"
}

remove_aports() {
	local aport age months age_limit
	months=$((APORT_TTL + months_to_release))
	age_limit=$(($(date +%s) - (2628000 * months)))
	setup_repo
	cd "$repo_dir" || exit
	git checkout -b aports-testing-ttl
	for aport in testing/*/APKBUILD; do
		echo "Getting timestamp for aport: $aport"
		age=$(git log --format='%ad' --date=format:'%s' -- "$aport" | tail -1)
		if [ "$age" -lt "$age_limit" ]; then
			echo "Removing: $aport"
			git rm -rf "${aport%/*}"
		fi
	done
	git commit -m "aports: remove aged packages from testing"
	if [ "${GITLAB_USER:-}" ] && [ "${GITLAB_TOKEN:-}" ]; then
		git push -f -o merge_request.create \
			-o merge_request.remove_source_branch \
			-o merge_request.title="$MR_TITLE" \
			origin aports-testing-ttl
	else
		echo "Missing GITLAB credentials skipping MR"
	fi
}

### Main ###

if [ "$#" = 1 ]; then
	case $1 in
		notify_maintainers) notify_maintainers;;
		remove_aports) remove_aports;;
		*) echo "Unknown option provided"; exit 1;;
	esac
else
	case "$(date +%m)" in
		02|08) notify_maintainers;;
		05|11) remove_aports;;
		*) echo "Skipping both runs."
	esac
fi
